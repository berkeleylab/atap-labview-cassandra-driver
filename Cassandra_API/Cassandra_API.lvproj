﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Base.lvclass" Type="LVClass" URL="../Classes/Base.lvclass"/>
		<Item Name="Cassandra_v3.lvclass" Type="LVClass" URL="../Classes/Cassandra_v3.lvclass"/>
		<Item Name="Cassandra_v4.lvclass" Type="LVClass" URL="../Classes/Cassandra_v4.lvclass"/>
		<Item Name="CQL-flags.ctl" Type="VI" URL="../../CQL-flags.ctl"/>
		<Item Name="Build Error.vi" Type="VI" URL="../Utils/Build Error.vi"/>
		<Item Name="CQL-read-header.vi" Type="VI" URL="../../CQL-read-header.vi"/>
		<Item Name="CQL-Write-basic-string-map.vi" Type="VI" URL="../../CQL-Write-basic-string-map.vi"/>
		<Item Name="Numeric to Error Ring.vi" Type="VI" URL="../Utils/Numeric to Error Ring.vi"/>
		<Item Name="Test.vi" Type="VI" URL="../Test.vi"/>
		<Item Name="Test_Read.vi" Type="VI" URL="../Test_Read.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="CQL-consistencylevel.ctl" Type="VI" URL="../../CQL-consistencylevel.ctl"/>
			<Item Name="CQL-conv-type-json.vi" Type="VI" URL="../../CQL-conv-type-json.vi"/>
			<Item Name="CQL-flags-v4.ctl" Type="VI" URL="../Classes/Cassandra_v4/Controls/CQL-flags-v4.ctl"/>
			<Item Name="CQL-frame.ctl" Type="VI" URL="../../CQL-frame.ctl"/>
			<Item Name="CQL-opcode.ctl" Type="VI" URL="../../CQL-opcode.ctl"/>
			<Item Name="CQL-option.ctl" Type="VI" URL="../../CQL-option.ctl"/>
			<Item Name="CQL-Read-basic-byte.vi" Type="VI" URL="../../CQL-Read-basic-byte.vi"/>
			<Item Name="CQL-Read-basic-bytes.vi" Type="VI" URL="../../CQL-Read-basic-bytes.vi"/>
			<Item Name="CQL-Read-basic-int.vi" Type="VI" URL="../../CQL-Read-basic-int.vi"/>
			<Item Name="CQL-Read-basic-short.vi" Type="VI" URL="../../CQL-Read-basic-short.vi"/>
			<Item Name="CQL-Read-basic-string-list.vi" Type="VI" URL="../../CQL-Read-basic-string-list.vi"/>
			<Item Name="CQL-Read-basic-string-multimap.vi" Type="VI" URL="../../CQL-Read-basic-string-multimap.vi"/>
			<Item Name="CQL-Read-basic-string.vi" Type="VI" URL="../../CQL-Read-basic-string.vi"/>
			<Item Name="CQL-read-data-from-byte-array.vi" Type="VI" URL="../../CQL-read-data-from-byte-array.vi"/>
			<Item Name="CQL-Read-query-result-metadata.vi" Type="VI" URL="../../CQL-Read-query-result-metadata.vi"/>
			<Item Name="CQL-Read-result-column-type.vi" Type="VI" URL="../../CQL-Read-result-column-type.vi"/>
			<Item Name="CQL-read-result.vi" Type="VI" URL="../../CQL-read-result.vi"/>
			<Item Name="CQL-request-response.ctl" Type="VI" URL="../../CQL-request-response.ctl"/>
			<Item Name="CQL-Write-basic-long-string.vi" Type="VI" URL="../../CQL-Write-basic-long-string.vi"/>
			<Item Name="CQL-Write-basic-string.vi" Type="VI" URL="../../CQL-Write-basic-string.vi"/>
			<Item Name="CQL_Version.ctl" Type="VI" URL="../Controls/CQL_Version.ctl"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>

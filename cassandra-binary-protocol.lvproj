﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Examples" Type="Folder">
			<Item Name="execute-arbitray-query.vi" Type="VI" URL="../execute-arbitray-query.vi"/>
			<Item Name="execute-insert-statement.vi" Type="VI" URL="../execute-insert-statement.vi"/>
			<Item Name="execute-read-statement.vi" Type="VI" URL="../execute-read-statement.vi"/>
		</Item>
		<Item Name="icons" Type="Folder">
			<Item Name="Apache-cassandra-icon-large.jpg" Type="Document" URL="../Apache-cassandra-icon-large.jpg"/>
			<Item Name="icon.jpg" Type="Document" URL="../icon.jpg"/>
		</Item>
		<Item Name="Read basic data types" Type="Folder">
			<Item Name="CQL-Read-basic-bigint.vi" Type="VI" URL="../CQL-Read-basic-bigint.vi"/>
			<Item Name="CQL-Read-basic-byte.vi" Type="VI" URL="../CQL-Read-basic-byte.vi"/>
			<Item Name="CQL-Read-basic-bytes.vi" Type="VI" URL="../CQL-Read-basic-bytes.vi"/>
			<Item Name="CQL-Read-basic-consistency.vi" Type="VI" URL="../CQL-Read-basic-consistency.vi"/>
			<Item Name="CQL-Read-basic-inet.vi" Type="VI" URL="../CQL-Read-basic-inet.vi"/>
			<Item Name="CQL-Read-basic-int.vi" Type="VI" URL="../CQL-Read-basic-int.vi"/>
			<Item Name="CQL-Read-basic-long-string.vi" Type="VI" URL="../CQL-Read-basic-long-string.vi"/>
			<Item Name="CQL-Read-basic-short-bytes.vi" Type="VI" URL="../CQL-Read-basic-short-bytes.vi"/>
			<Item Name="CQL-Read-basic-short.vi" Type="VI" URL="../CQL-Read-basic-short.vi"/>
			<Item Name="CQL-Read-basic-string-list.vi" Type="VI" URL="../CQL-Read-basic-string-list.vi"/>
			<Item Name="CQL-Read-basic-string-map.vi" Type="VI" URL="../CQL-Read-basic-string-map.vi"/>
			<Item Name="CQL-Read-basic-string-multimap.vi" Type="VI" URL="../CQL-Read-basic-string-multimap.vi"/>
			<Item Name="CQL-Read-basic-string.vi" Type="VI" URL="../CQL-Read-basic-string.vi"/>
			<Item Name="CQL-Read-basic-uuid.vi" Type="VI" URL="../CQL-Read-basic-uuid.vi"/>
		</Item>
		<Item Name="typedefs" Type="Folder">
			<Item Name="CQL-consistencylevel.ctl" Type="VI" URL="../CQL-consistencylevel.ctl"/>
			<Item Name="CQL-flags.ctl" Type="VI" URL="../CQL-flags.ctl"/>
			<Item Name="CQL-frame.ctl" Type="VI" URL="../CQL-frame.ctl"/>
			<Item Name="CQL-opcode.ctl" Type="VI" URL="../CQL-opcode.ctl"/>
			<Item Name="CQL-option.ctl" Type="VI" URL="../CQL-option.ctl"/>
			<Item Name="CQL-request-response.ctl" Type="VI" URL="../CQL-request-response.ctl"/>
		</Item>
		<Item Name="Write basic data type" Type="Folder">
			<Item Name="CQL-Write-basic-long-string.vi" Type="VI" URL="../CQL-Write-basic-long-string.vi"/>
			<Item Name="CQL-Write-basic-string-list.vi" Type="VI" URL="../CQL-Write-basic-string-list.vi"/>
			<Item Name="CQL-Write-basic-string-map.vi" Type="VI" URL="../CQL-Write-basic-string-map.vi"/>
			<Item Name="CQL-Write-basic-string.vi" Type="VI" URL="../CQL-Write-basic-string.vi"/>
		</Item>
		<Item Name="Cassandra-driver.txt" Type="Document" URL="../Cassandra-driver.txt"/>
		<Item Name="Cassandra-driver.vi" Type="VI" URL="../Cassandra-driver.vi"/>
		<Item Name="CQL-close-DB.vi" Type="VI" URL="../CQL-close-DB.vi"/>
		<Item Name="CQL-conv-type-json.vi" Type="VI" URL="../CQL-conv-type-json.vi"/>
		<Item Name="CQL-open-DB.vi" Type="VI" URL="../CQL-open-DB.vi"/>
		<Item Name="CQL-query.vi" Type="VI" URL="../CQL-query.vi"/>
		<Item Name="CQL-read-data-from-byte-array.vi" Type="VI" URL="../CQL-read-data-from-byte-array.vi"/>
		<Item Name="CQL-read-header.vi" Type="VI" URL="../CQL-read-header.vi"/>
		<Item Name="CQL-Read-query-result-metadata.vi" Type="VI" URL="../CQL-Read-query-result-metadata.vi"/>
		<Item Name="CQL-Read-result-column-type.vi" Type="VI" URL="../CQL-Read-result-column-type.vi"/>
		<Item Name="CQL-read-result.vi" Type="VI" URL="../CQL-read-result.vi"/>
		<Item Name="CQL-use-keyspace.vi" Type="VI" URL="../CQL-use-keyspace.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Open a Document on Disk.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open a Document on Disk.vi"/>
				<Item Name="Open Acrobat Document.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open Acrobat Document.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Cassandra-v3-lib" Type="Source Distribution">
				<Property Name="Bld_buildCacheID" Type="Str">{BD8E84A8-61A4-402D-8A9E-6E6E7F6C384E}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Cassandra-v3-lib</Property>
				<Property Name="Bld_excludedDirectory[0]" Type="Path">vi.lib</Property>
				<Property Name="Bld_excludedDirectory[0].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[1]" Type="Path">resource/objmgr</Property>
				<Property Name="Bld_excludedDirectory[1].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[2]" Type="Path">/C/ProgramData/National Instruments/InstCache/13.0</Property>
				<Property Name="Bld_excludedDirectory[3]" Type="Path">instr.lib</Property>
				<Property Name="Bld_excludedDirectory[3].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[4]" Type="Path">user.lib</Property>
				<Property Name="Bld_excludedDirectory[4].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectoryCount" Type="Int">5</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/cassandra-lib</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{57293F17-7F29-4698-B6EC-5947F073CB26}</Property>
				<Property Name="Bld_version.minor" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/cassandra-lib</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/cassandra-lib/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{0319A27D-83B7-489D-92D1-4F24062F9D40}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[10].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/Read basic data types</Property>
				<Property Name="Source[10].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[10].type" Type="Str">Container</Property>
				<Property Name="Source[11].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/typedefs</Property>
				<Property Name="Source[11].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[11].type" Type="Str">Container</Property>
				<Property Name="Source[12].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/Write basic data type</Property>
				<Property Name="Source[12].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[12].type" Type="Str">Container</Property>
				<Property Name="Source[13].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/Examples/execute-arbitray-query.vi</Property>
				<Property Name="Source[13].sourceInclusion" Type="Str">Exclude</Property>
				<Property Name="Source[13].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref"></Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref"></Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">VI</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref"></Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].type" Type="Str">VI</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref"></Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].type" Type="Str">VI</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref"></Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].type" Type="Str">VI</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref"></Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].type" Type="Str">VI</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref"></Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].type" Type="Str">VI</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref"></Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">14</Property>
			</Item>
		</Item>
	</Item>
</Project>
